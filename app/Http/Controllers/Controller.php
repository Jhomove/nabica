<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $vehiculos = [
            [
                'id' => 1,
                'nombre' => 'AMG',
                'img' => 'images/amg.png',
                'alt' => 'AMG'
            ],
            [
                'id' => 2,
                'nombre' => 'CLASE C CABRIO',
                'img' => 'images/c-cabrio.png',
                'alt' => 'CLASE C CABRIO'
            ],
            [
                'id' => 3,
                'nombre' => 'CLA COUPE',
                'img' => 'images/cla-coupe.png',
                'alt' => 'CLA COUPE'
            ],
            [
                'id' => 4,
                'nombre' => 'CLASE B',
                'img' => 'images/clase-b.png',
                'alt' => 'CLASE B'
            ],
            [
                'id' => 5,
                'nombre' => 'CLASE V',
                'img' => 'images/clase-v.png',
                'alt' => 'CLASE V'
            ],
            [
                'id' => 6,
                'nombre' => 'CLASE G',
                'img' => 'images/clase-g.png',
                'alt' => 'CLASE G'
            ]
        ];
        return view('welcome', compact('vehiculos'));
    }

    public function vehicles()
    {
        $vehiculos = [
            [
                'id' => 1,
                'nombre' => 'AMG 2',
                'img' => 'images/amg.png',
                'alt' => 'AMG'
            ],
            [
                'id' => 2,
                'nombre' => 'CLASE C CABRIO 2',
                'img' => 'images/c-cabrio.png',
                'alt' => 'CLASE C CABRIO'
            ],
            [
                'id' => 3,
                'nombre' => 'CLA COUPE 2',
                'img' => 'images/cla-coupe.png',
                'alt' => 'CLA COUPE'
            ],
            [
                'id' => 4,
                'nombre' => 'CLASE B 2',
                'img' => 'images/clase-b.png',
                'alt' => 'CLASE B'
            ],
            [
                'id' => 5,
                'nombre' => 'CLASE V 2',
                'img' => 'images/clase-v.png',
                'alt' => 'CLASE V'
            ],
            [
                'id' => 6,
                'nombre' => 'CLASE G 2',
                'img' => 'images/clase-g.png',
                'alt' => 'CLASE G'
            ]
        ];

        return response()->json($vehiculos, 200);
    }
}
