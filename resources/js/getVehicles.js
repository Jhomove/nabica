var divs = $('.container-vehicles .card');
var now = 0;
$.ajax({
    url: '/vehicles',
    success: function(vehiculos) {
        vehiculos.forEach(vehiculo => {
            const container = $('.container-card-vehicles').append(
                `
                <div class="card col-md-3 col-sm-8 col-12 m-4" style="width: 18rem;">
                    <img class="card-img-top" src="${vehiculo.img}" alt="${vehiculo.alt}">
                    <div class="card-body d-flex justify-content-center">
                    <h5 class="card-title">${vehiculo.nombre}</h5>
                    </div>
                    <a href="/vehicles/${vehiculo.id}" class="btn btn-primary">Ver mas</a>
                </div>
                `
            );
            divs = $('.container-vehicles .card');
            now = 0;
        });
    },
    error: function() {
        console.log("No se ha podido obtener la información");
    }
});
divs.eq(now).removeClass('d-none');
const next = () => {
    $('.container-card-vehicles').animate({ scrollLeft: '+=150' }, 500);
    return false;
}

const previous = () => {
    $('.container-card-vehicles').animate({ scrollLeft: '-=150' }, 500);
    return false;
}
window.next = next;
window.previous = previous;