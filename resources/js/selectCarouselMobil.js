$('#carouselExampleIndicators').on('slid.bs.carousel', function(e) {
    const itemSelected = $('.carousel-indicators .active[data-target="#carouselExampleIndicators"]').data('slide-to');
    $('.mobile-info-text').addClass('d-none');
    $(`.mobile-info-text[data-slide-to="${itemSelected}"]`).removeClass('d-none');
});