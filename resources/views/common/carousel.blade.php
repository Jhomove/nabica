<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active" onchange="cambiarOpcion()"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1" onchange="cambiarOpcion()"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2" onchange="cambiarOpcion()"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="{{asset('images/banner-C200-avantgarde.jpg')}}" alt="First slide">
        <div class="container-standard">
            <div class="text">
                <h1>
                    Clase B 200 Progressive
                </h1>
                <p>
                    Mercedes-Benz clase B, Hatch Back familiar por excelencia, con dimensiones de SUV compacta.
                </p>
                <a class="button" href="#">Ver vehículo</a>

            </div>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{asset('images/banner-clase-a-200-progressive.jpg')}}" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{asset('images/banner01.jpg')}}" alt="Third slide">
      </div>
      <a class="carousel-control-prev d-none" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next d-none" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</div>
<section class="mobile-info">
    <div class="container-standard">
            <div class="mobile-info-text" data-slide-to="0">
                <h1>Clase B 200 Progressive</h1>
                <p>Mercedes-Benz clase B, Hatch Back familiar por excelencia, con dimensiones de SUV compacta.</p>
                <a class="button" href="http://phplaravel-248778-1330191.cloudwaysapps.com/automoviles/portafolio/clase-b-200-progressive">Ver vehículo</a>
            </div>
            <div class="mobile-info-text d-none" data-slide-to="1">
                <h1>Clase A 200 Progressive</h1>
                <p>Mercedes-Benz con el Clase A: turismo, compacto de gama alta, deportividad, confort y elegancia.</p>
                <a class="button" href="http://phplaravel-248778-1330191.cloudwaysapps.com/automoviles/portafolio/clase-a-200-progressive">Ver vehículo</a>
            </div>
            <div class="mobile-info-text d-none" data-slide-to="2">
                <h1>C200 Avantgarde</h1>
                <p>Mercedes-Benz clase C presenta una berlina, elegante, confortable, que genera prestigio y dinamismo.</p>
                <a class="button" href="http://phplaravel-248778-1330191.cloudwaysapps.com/automoviles/portafolio/c200-avantgarde">Ver vehículo</a>
            </div>
    </div>
</section>