<nav id="menu-main" class="menu-main">
    <div class="data-protection d-none d-lg-flex">
        <a href="http://phplaravel-248778-1330191.cloudwaysapps.com/pdfs/Manual-de-prevención-y-control-de-riesgo-de-lavado-de-activos-y-financiación-del-terrorismo.pdf">
            <span>
                Manual de prevención de riesgo LA/FT
            </span>
        </a>
        <a on="tap:modal-terms" tabindex="0">
            <span>
                <svg viewBox="0 0 13 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="icon">
                    <path d="M13,14 L0,14 L0,0 L13,0 L13,14 Z M12,1 L1,1 L1,13 L12,13 L12,1 Z M7.4,10.1 L7.4,5.9 L5.2,5.9 L5.2,6.8 L6.2,6.8 L6.2,10.1 L5.1,10.1 L5.1,11 L8.3,11 L8.3,10.1 L7.4,10.1 Z M6.6,4.9 C7.1,4.9 7.5,4.5 7.5,4 C7.5,3.4 7.1,3 6.6,3 C6.1,3 5.7,3.4 5.7,4 C5.7,4.5 6.1,4.9 6.6,4.9 Z" id="aba"></path>
                </svg>
            </span>
            <span>
                Términos y Condiciones
            </span>
        </a>
    </div>

    <div class="left">
        <a href="#">
            <img src="logo.svg" alt="logo" class="logo">
            <img src="mb_texto_no.svg" alt="logo" class="claim">
        </a>
        <a href="#">
            <img src="floresta-logo.svg" alt="floresta-logo" class="logo-f">
        </a>
    </div>
    <div class="right">
        <div class="links">
            <ul>
                <li>
                    <a href="">
                        <img src="icon-car-24.svg" alt="icon-car">
                    </a>
                    <a href="#">Automóviles</a>
                </li>
                <li><a href="http://phplaravel-248778-1330191.cloudwaysapps.com/automoviles/usados/portafolio">Usados</a></li>
                <li><a href="http://phplaravel-248778-1330191.cloudwaysapps.com/automoviles/accesorios">Boutique</a></li>
                <li><a href="http://phplaravel-248778-1330191.cloudwaysapps.com/automoviles/posventa">Posventa</a></li>
                <li> <a href="https://livetour.istaging.com/07059bda-2ce7-4db4-8602-31ce2aaf6b05?index=8">Vitrina
                    virtual</a></li>
                <li><a href="http://phplaravel-248778-1330191.cloudwaysapps.com/automoviles/contacto">Contacto</a></li>
                
            </ul>
        </div>
    </div>
</nav>