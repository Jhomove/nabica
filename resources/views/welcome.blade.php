<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body>
        <div id="container-menu-main">
            <div class="container-standard">
                @include('common.header')
            </div>
        </div>
        <div id="container-main-content">
            <section id="container-carousel-banner-home">
                @include('common.carousel')
            </section>
            <div class="container-vehicles d-flex flex-row justify-content-center">
                <div class="previous">
                    <button class="btn text-light previous" onclick="previous()">&laquo; Previous</button>
                </div>
                <div class="container-card-vehicles col-8 mt-4 d-flex flex-row justify-content-center">
                    @foreach ($vehiculos as $key => $vehiculo)
                        @component('components.vehicle-card')
                            @slot('vehiculo', $vehiculo)
                        @endcomponent
                    @endforeach
                </div>
                <div class="next">
                    <button class="btn text-light next" onclick="next()">Next &raquo;</button>
                </div>
            </div>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
